export function getUrlParameter(name) {
  // Optional: implement this using a fallback to support IE11
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.has(name) ? urlParams.get(name) : null;
}

export function isDomainAllowed(url, allowedDomains) {
  const urlObject = new URL(url);
  const isAllowed = allowedDomains.indexOf(urlObject.host) > -1;
  return isAllowed;
}

export function cleanString(originalString) {
  if (!originalString) {
    return null;
  }
  const strippedString = originalString.replace(/(<([^>]+)>)/gi, "");
  return strippedString;
}

export default getUrlParameter;
