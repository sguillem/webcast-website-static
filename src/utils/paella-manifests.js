/**
 * Generate a manifest (data.json) for stream using the provided title, camera and slides source.
 *
 * @param {string} title The title of the stream
 * @param {string} cameraSrc URL of the camera's stream (presenter)
 * @param {string} slidesSrc URL of the slides's stream (presentation)
 * @returns
 */
export function createLiveCameraSlidesManifest(title, cameraSrc, slidesSrc) {
  const jsonContent = `{
    "metadata": {
      "title": "${title}",
      "preview": "/images/preview.jpeg",
      "duration": 1
    },
    "streams": [
      {
        "sources": {
          "hlsLive": [
            {
              "src": "${cameraSrc}",
              "mimetype": "video/mp4",
              "isLiveStream": true,
              "res": {
                "w": "1920",
                "h": "1080"
              }
            }
          ]
        },
        "content": "presenter",
        "role": "mainAudio"
      },
      {
        "sources": {
          "hlsLive": [
            {
              "src": "${slidesSrc}",
              "mimetype": "video/mp4",
              "isLiveStream": true,
              "res": {
                "w": "1920",
                "h": "1080"
              }
            }
          ]
        },
        "content": "presentation"
      }
    ]
  }`;
  return jsonContent;
}

/**
 * Generate a manifest (data.json) for stream using the provided title, camera source.
 *
 * @param {string} title The title of the stream
 * @param {string} cameraSrc URL of the camera's stream (presenter)
 * @returns
 */
export function createLiveCameraManifest(title, cameraSrc) {
  const jsonContent = `{
    "metadata": {
      "title": "${title}",
      "preview": "/images/preview.jpeg",
      "duration": 1
    },
    "streams": [
      {
        "sources": {
          "hlsLive": [
            {
              "src": "${cameraSrc}",
              "mimetype": "video/mp4",
              "isLiveStream": true,
              "res": {
                "w": "1920",
                "h": "1080"
              }
            }
          ]
        },
        "content": "presenter",
        "role": "mainAudio"
      }
    ]
  }`;
  return jsonContent;
}

/**
 * Generate a manifest (data.json) for stream using the provided title and slides source.
 *
 * @param {string} title The title of the stream
 * @param {string} slidesSrc URL of the slides's stream (presentation)
 * @returns
 */
export function createLiveSlidesManifest(title, slidesSrc) {
  const jsonContent = `{
    "metadata": {
      "title": "${title}",
      "preview": "/images/preview.jpeg",
      "duration": 1
    },
    "streams": [
      {
        "sources": {
          "hlsLive": [
            {
              "src": "${slidesSrc}",
              "mimetype": "video/mp4",
              "isLiveStream": true,
              "res": {
                "w": "1920",
                "h": "1080"
              }
            }
          ]
        },
        "content": "presentation",
        "role": "mainAudio"
      }
    ]
  }`;
  return jsonContent;
}
