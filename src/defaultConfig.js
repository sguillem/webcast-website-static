/*

 */
/**
 * Set which default streams should be used:
 * - camera: only camera
 * - slides: only slides
 * - camera_slides: camera and slides
 * @type {string}
 */
export const DEFAULT_STREAMS = "camera_slides";

/**
 * URL for the camera stream
 * @type {string}
 */
export const DEFAULT_CAMERA_URL =
  "https://wowza.cern.ch/livehd/smil:1300908_camera_all.smil/playlist.m3u8";
/**
 *
 * @type {string}
 */
export const DEFAULT_SLIDES_URL =
  "https://wowza.cern.ch/livehd/smil:1300908_slides_all.smil/playlist.m3u8";
/**
 * If no parameters are passed, the default title will be displayed
 * @type {string}
 */
// export const DEFAULT_TITLE = "Test Stream";
export const DEFAULT_TITLE = "Directorate Meeting - July 2023";

/**
 * List of allowed domains to load streams from
 * @type {string}
 */
export const ALLOWED_DOMAINS = [
  "wowza.cern.ch", // CERN
  "wowza04.cern.ch",
  "wowza05.cern.ch",
  "wowza06.cern.ch",
  "wowzaqaedge.cern.ch", // CERN QA
  "cern2.vo.llnwd.net", // Limelight
];
