import React, { useEffect, useRef } from "react";
import getBasicPluginContext from "paella-basic-plugins";
import { Paella, utils } from "paella-core";
import {
  ALLOWED_DOMAINS,
  DEFAULT_CAMERA_URL,
  DEFAULT_SLIDES_URL,
  DEFAULT_STREAMS,
  DEFAULT_TITLE,
} from "defaultConfig";
import getCernCustomPluginsContext from "plugins";
import {
  createLiveCameraManifest,
  createLiveCameraSlidesManifest,
  createLiveSlidesManifest,
} from "utils/paella-manifests";
import { getUrlParameter, cleanString, isDomainAllowed } from "utils/urls";

export async function customGetManifestFileUrlFunction() {
  console.log("Using custom getManifestFileUrl function");
  return "";
}

export async function customLoadVideoManifestFunction() {
  console.debug("Using custom loadVideoManifest function");
  function getManifest() {
    let usedType = DEFAULT_STREAMS;
    let usedTitle = DEFAULT_TITLE;
    let usedCameraUrl = DEFAULT_CAMERA_URL;
    let usedSlidesUrl = DEFAULT_SLIDES_URL;

    console.debug("Using custom getManifest function");

    const cameraUrlFromQueryParams = cleanString(getUrlParameter("camera"));
    const slidesUrlFromQueryParams = cleanString(getUrlParameter("slides"));
    const title = cleanString(getUrlParameter("title"));

    if (cameraUrlFromQueryParams && slidesUrlFromQueryParams) {
      if (
        isDomainAllowed(cameraUrlFromQueryParams, ALLOWED_DOMAINS) &&
        isDomainAllowed(slidesUrlFromQueryParams, ALLOWED_DOMAINS)
      ) {
        usedType = "camera_slides";
        usedCameraUrl = cameraUrlFromQueryParams;
        usedSlidesUrl = slidesUrlFromQueryParams;
      }
    }
    if (cameraUrlFromQueryParams && !slidesUrlFromQueryParams) {
      if (isDomainAllowed(cameraUrlFromQueryParams, ALLOWED_DOMAINS)) {
        usedType = "camera";
        usedCameraUrl = cameraUrlFromQueryParams;
      }
    }
    if (!cameraUrlFromQueryParams && slidesUrlFromQueryParams) {
      if (isDomainAllowed(slidesUrlFromQueryParams, ALLOWED_DOMAINS)) {
        usedType = "slides";
        usedSlidesUrl = slidesUrlFromQueryParams;
      }
    }
    if (title) {
      usedTitle = title;
    }

    const manifests = {
      camera_slides: () =>
        createLiveCameraSlidesManifest(usedTitle, usedCameraUrl, usedSlidesUrl),
      camera: () => createLiveCameraManifest(usedTitle, usedCameraUrl),
      slides: () => createLiveSlidesManifest(usedTitle, usedSlidesUrl),
    };
    const result = manifests[usedType]();
    return result;
  }
  const manifest = getManifest();
  return JSON.parse(manifest);
}

export async function defaultLoadConfigFunction(configUrl, player) {
  console.log("Using CUSTOM configuration loading function.");
  console.log(player);
  const response = await fetch(configUrl);
  return response.json();
}

export async function customtGetVideoIdFunction() {
  console.log("Using CUSTOM getVideoId function");
  return "";
}

// repoUrl: the value specified in initParams.repositoryUrl
// videoId: the video identifier returned by initParams.getVideoId()
export async function defaultGetManifestUrlFunction(
  repoUrl,
  videoId,
  config,
  player,
) {
  console.log("Using custom getManifestUrl function");
  console.log(repoUrl, videoId, config, player);
  return "";
}

export async function defaultLoadVideoManifestFunction(
  videoManifestUrl,
  config,
  player,
) {
  console.log("Using CUSTOM loadVideoManifest function");
  console.log(config, player);
  const response = await fetch(videoManifestUrl);
  return response.json();
}

export default function Player() {
  const videoRef = useRef(null);
  const playerRef = useRef(null);

  // https://webcast.web.cern.ch/event/i991843?app=livehd&camera&slides

  useEffect(() => {
    const loadPaella = async () => {
      console.debug("Initializing Paella Player plugins context...");
      if (!playerRef.current) {
        const videoElement = videoRef.current;
        if (!videoElement) {
          return;
        }

        const initParams = {
          // Initialization callbacks
          // loadConfig: defaultLoadConfigFunction, // overrides the config.json file load
          getVideoId: customtGetVideoIdFunction, // get the video identifier
          // getManifestUrl: defaultGetManifestUrlFunction, // get the video manifest url
          getManifestFileUrl: customGetManifestFileUrlFunction, // get the full manifest file url
          loadVideoManifest: customLoadVideoManifestFunction, // get the manifest file content
          customPluginContext: [
            getCernCustomPluginsContext(),
            getBasicPluginContext(),
          ],
        };
        console.log("Initializing Paella Player plugins context... OK");
        const player = new Paella(videoRef.current, initParams);
        console.log("Initializing Paella Player... OK");

        try {
          await player.loadManifest();
          console.log("Loading video manifest... OK");
          await utils.loadStyle("style.css");
          console.debug("Loading styles from Paella Core API... OK");
          playerRef.current = player;
        } catch (error) {
          console.log(error);
        }
      }
    };
    loadPaella();
  }, []);

  // Remove the player when the functional component unmounts
  React.useEffect(() => {
    const player = playerRef.current;
    return () => {
      if (player) {
        console.log("Unmount player");
        playerRef.current = null;
      }
    };
  }, [playerRef]);

  if (!videoRef && !videoRef.current) {
    return <div>Loading player...</div>;
  }

  return (
    <div
      data-testid="player-container"
      ref={videoRef}
      className="player-container"
      style={{
        minHeight: "600px",
      }}
    />
  );
}
