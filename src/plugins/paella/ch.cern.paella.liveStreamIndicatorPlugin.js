/* eslint-disable no-param-reassign */
/* eslint-disable max-classes-per-file */
import { Canvas, CanvasPlugin, createElementWithHtmlText } from "paella-core";
import * as img_url from "./icons/live-icon.png";

// Canvas implementation
export class LiveStreamIndicatorCanvas extends Canvas {
  /**
   * This  class displays an image indicating whether the stream is live or not.
   *
   * @param {*} player
   * @param {*} videoContainer
   * @param {*} stream
   */
  constructor(player, videoContainer, stream) {
    super("div", player, videoContainer);
    this.stream = stream;
    this.parentContainer = videoContainer;
    this.indicatorCount = 0;
  }

  /**
   * When the plugin is loaded, this code will be executed.
   * It will display the image of the live stream indicator if the stream is live.
   *
   * @param {*} player
   */
  async loadCanvas() {
    let isLiveStream = false;
    console.log("Loading the live stream indicator canvas");

    const streamSources = this.stream.sources;

    const setStreamAsLive = (key, value) => {
      const tempIsLiveStream = value[0].isLiveStream;
      if (tempIsLiveStream) {
        isLiveStream = tempIsLiveStream;
      }
    };

    Object.keys(streamSources).forEach((key) => {
      setStreamAsLive(key, streamSources[key]);
    });

    if (isLiveStream) {
      if (this.indicatorCount === 0) {
        console.log("Stream is live. Displaying the live stream indicator");
        createElementWithHtmlText(
          `<div><img class="live-image-plugin" src="${img_url.default}"/></div>`,
          this.parentContainer,
        );
        this.indicatorCount += 1;
      }
    }
  }
}

// Canvas plugin definition
export default class LiveStreamIndicatorPlugin extends CanvasPlugin {
  // eslint-disable-next-line class-methods-use-this
  get parentContainer() {
    return "videoContainer"; // or videoContainer
  }

  isCompatible(stream) {
    if (!Array.isArray(stream.canvas) || stream.canvas.length === 0) {
      // By default, the default canvas is HTML video canvas
      this.stream = stream;
      return true;
    }

    return super.isCompatible(stream);
  }

  getCanvasInstance(videoContainer) {
    return new LiveStreamIndicatorCanvas(
      this.player,
      videoContainer,
      this.stream,
    );
  }
}
