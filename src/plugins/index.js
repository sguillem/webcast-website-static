import requireContext from "require-context.macro";

export default function getCernCustomPlugins() {
  return requireContext("./paella", true, /\.js/);
}
