import React from "react";
import "./App.css";
import {
  Form,
  Grid,
  Header,
  Image,
  Segment,
  Container,
} from "semantic-ui-react";
import packageJson from "../package.json";
import { DEFAULT_TITLE } from "defaultConfig";
import HomePage from "pages/HomePage/HomePage";
import { getUrlParameter } from "utils/urls";

function App() {
  const title = getUrlParameter("title");

  return (
    <Container>
      <Grid columns={1} padded>
        <Grid.Row>
          <Grid.Column>
            <Header as="h2" inverted>
              <Image src="/images/cern_80_white.png" />
              <Header.Content>
                Webcast
                <Header.Subheader>{title || DEFAULT_TITLE}</Header.Subheader>
              </Header.Content>
            </Header>
            <HomePage />
            <Segment inverted>
              <Form inverted readOnly>
                <p>Click on the video to start/pause the stream</p>
              </Form>
            </Segment>
            <Segment inverted>
              <p>
                <a
                  href="http://cern.ch/copyright"
                  target="_blank"
                  title="CERN copyright policy"
                  rel="noreferrer"
                >
                  {2019} - {new Date().getFullYear()} © Copyright CERN
                </a>{" "}
                - Version {packageJson.version}
              </p>
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
}

export default App;
