import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

jest.mock("paella-basic-plugins", () => jest.fn());
jest.mock("paella-core", () => ({
  Paella: jest.fn().mockImplementation(() => {
    return {};
  }),
  useParams: () => ({
    id: "1",
  }),
}));

describe("App component tests", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    render(<App />, div);
    let element = screen.getByText(/Webcast/i);
    expect(element).toBeInTheDocument();
    element = screen.getByTestId(/player-container/i);
    expect(element).toBeInTheDocument();
    element = screen.getByText(
      /Click on the video to start\/pause the stream/i,
    );
    expect(element).toBeInTheDocument();
  });
});
