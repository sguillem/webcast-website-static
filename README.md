# Static Webcast Website

<p align="center"><img src="live-bw.png" alt="Video logo" height="200"></p>

Static version of the Webcast Website that allows it's users to watch a webcast directly.

It is built using React and Paella Player 7.

## Usage

By default, the parameters used will be those in the `defaultConfig.js` file.

1. `DEFAULT_STREAMS`: (camera|slides|camera_slides) (string)
2. `DEFAULT_CAMERA_URL`: (string)
3. `DEFAULT_SLIDES_URL`: (string)
4. `DEFAULT_TITLE`: (string)
5. `ALLOWED_DOMAINS`: (string array)

### Usage with default configuration

```
http://localhost:3000/
```

### Usage with query parameters
```
http://localhost:3000/?title=This is the title&camera=https://wowza.cern.ch/vod/_definist_/smil:Video/Public/WebLectures/2017/635414c2/635414c2_desktop_camera.smil/playlist.m3u8&slides=https://wowza.cern.ch/vod/_definist_/smil:Video/Public/WebLectures/2017/635414c2/635414c2_desktop_slides.smil/playlist.m3u8

```

The website expects 3 parameters:

* `title`: Will display the title of the event
* `camera`: URL of the camera stream
* `slides`: URL of the slides stream

If any of these parameters is missing, the website will work, but it will hide some parts of the website.

## Development

### Requirements

- Node 16.15.x
- Yarn 1.22.x

### Install dependencies

```
yarn
```

### Run development server

```
yarn start
```

## Attributions

- Logo: <a href="https://www.flaticon.es/iconos-gratis/television" title="televisión iconos">Icon created by Freepik - Flaticon</a>